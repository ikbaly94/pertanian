<!DOCTYPE html>
<!-- 
Template Name: BRILLIANT Bootstrap Admin Template
Version: 4.5.6
Author: WebThemez
Website: http://www.webthemez.com/ 
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta content="" name="description" />
    <meta content="webthemez" name="author" />
    <title>BRILLIANT Free Bootstrap Admin Template - WebThemez</title>
    <!-- Bootstrap Styles-->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- Morris Chart Styles-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- Custom Styles-->
    <link href="assets/css/custom-styles.css" rel="stylesheet" />
    <!-- Google Fonts-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" href="assets/js/Lightweight-Chart/cssCharts.css"> 
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="j.html"><strong><i class="icon fa fa-plane"></i>PERTANIAN</strong></a>
                
        <div id="sideNav" href="">
        <i class="fa fa-bars icon"></i> 
        </div>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                   
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        </li>
                        <li class="divider"></li>
                        <li><a href="../logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
        </nav>
        <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">

                   <li> 
                    <a href="index.php"><i class="fa fa-dashboard"></i> Dashbord</a>
                    </li>
                     <li>
                        <a href="#"><i class="fa fa-sitemap"></i> Admin<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="user.php">User</a>
                            </li>
                            <li>
                                <a href="admin.php">Admin</a>
                            </li>
                            </ul>
                        </li>                    
                    <li>
                        <a href="#"><i class="fa fa-sitemap"></i> Padi<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="table.html">Tambah Padi</a>
                            </li>
                            <li>
                                <a href="padiT.php">Tabel Luas Tanah</a>
                            </li>
                            </ul>
                        </li>
                     <li>
                        <a href="#" ><i class="fa fa-sitemap"></i> Jagung<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="j.html">Tambah Jagung</a>
                            </li>
                            <li>
                                <a href="jagungT.php">Tabel Luas Tanah</a>
                            </li>
                            </ul>
                        </li>   
                            
                     <li>
                        <a href="#"><i class="fa fa-sitemap"></i> Kedelai<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                           <li>
                                <a href="tab-panel.html">Tambah Kedelai</a>
                            </li>
                            <li>
                                <a href="kedelaiT.php">Tabel Luas Tanah</a>
                            </li>
                            </ul>
                        </li>
                    
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
      
        <div id="page-wrapper">
          <div class="header"> 
                        
                        
                    <?php
include "koneksi.php";
//"SELECT pl.no, pl.luas, pl.thn, pn.kota FROM tbl_lunah pl, tbl_kota pn WHERE pl.no = pn.no_kota"
$query=mysqli_query($con,"SELECT pl.no, pl.luas, pl.thn, pn.kota FROM tbl_lunah pl, tbl_kota pn WHERE pl.no_kota = pn.no_kota and pl.no_biji=3");

?>
<html>  

<body><center><br/>
<h1>DATA PERTANIAN KEDELAI</h1></center><br/><br/>
<center>
<table border="1" class="table table-striped table-bordered" id="table">
<thead>

    <tr>
        <th>No</th>
        <th>KOTA</th>
        <th>TAHUN</th>
        <th>LUAS TANAH</th>
    </tr>
</thead>
<tbody>
<?php
while($data=mysqli_fetch_array($query)){
?>
    <tr>
        <td><?php echo $data['no'];?></td>
        <td><?php echo $data['kota'];?></td>
        <td><?php echo $data['thn'];?></td>
        <td><?php echo $data['luas'];?></td>
        <td>
            <a href="editTampilan.php?no=<?php echo $data['no'] ?>"><button>Edit</button></a>
            <a href="hapusKed.php?no=<?php echo $data['no'] ?>" onClick=" return confirm('apakah anda yakin ingin menghapusnya?')" ><button>Hapus</button></a>
        </td>
    </tr>

<?php
}
?>
</tbody>
<tfoot></tfoot>
</table>
</div>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        $('#table').DataTable();
    });
</script>
</from>
</html>              
 
                                    
        </div>
           
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
    <!-- JS Scripts-->
    <!-- jQuery Js -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- Bootstrap Js -->
    <script src="assets/js/bootstrap.min.js"></script>
     
    <!-- Metis Menu Js -->
    <script src="assets/js/jquery.metisMenu.js"></script>
    <!-- Morris Chart Js -->
    <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
    
    
    <script src="assets/js/easypiechart.js"></script>
    <script src="assets/js/easypiechart-data.js"></script>
    
     <script src="assets/js/Lightweight-Chart/jquery.chart.js"></script>
    
    <!-- Custom Js -->
    <script src="assets/js/custom-scripts.js"></script>

      
    <!-- Chart Js -->
    <script type="text/javascript" src="assets/js/Chart.min.js"></script>  
    <script type="text/javascript" src="assets/js/chartjs.js"></script> 

</body>

</html>