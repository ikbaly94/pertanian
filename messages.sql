-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 04, 2017 at 10:00 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `messages`
--

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `username`, `password`) VALUES
(1, 'admin', 'c93ccd78b2076528346216b3b2f701e6'),
(5, 'DPPKAD', '00d556b096d6e869d20e337830c280f0'),
(6, 'abbay', '00d556b096d6e869d20e337830c280f0'),
(7, 'cahya', '781e5e245d69b566979b86e28d23f2c7');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `no` int(11) NOT NULL AUTO_INCREMENT,
  `Kode_Surat` varchar(25) DEFAULT NULL,
  `kategori` varchar(10) NOT NULL,
  `tgl` varchar(30) NOT NULL,
  `no_surat` varchar(50) NOT NULL,
  `dari` varchar(100) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `ket` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `unit` varchar(255) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`no`, `Kode_Surat`, `kategori`, `tgl`, `no_surat`, `dari`, `perihal`, `ket`, `image`, `unit`) VALUES
(1, '98867564', 'masuk', '08/04/2016', '2000000', 'sutomo', 'satu dua', 'test cek', 'logokbb.png', 'dinas'),
(2, '444444444', 'keluar', '11/01/2016', '5634356', 'jarwo', 'siapsiap', 'pasti', 'gr.png', '76767'),
(11, '324242', 'masuk', '08/17/2017', '0009005', 'ahyar', 'afa', 'hjhk', 'gr.png', '676'),
(12, '324242', 'masuk', '09/01/2017', '0009005', 'ahyar', 'afa', 'vavahj', 'logokbb.png', '76767'),
(13, '324242', 'masuk', '08/01/2017', '0009005', 'ahyar', 'afa', 'bhbfhdbvbhjri', 'logokbb.png', '76767'),
(14, '324242', 'masuk', '08/01/2017', '0009005', 'ahyar', 'afa', 'vsgdh', 'logokbb.png', '76767');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
