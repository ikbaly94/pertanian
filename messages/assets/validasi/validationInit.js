﻿function formValidation() {
    "use strict";
    /*----------- BEGIN validationEngine CODE -------------------------*/
    $('#popup-validation').validationEngine();
    /*----------- END validationEngine CODE -------------------------*/

    /*----------- BEGIN validate CODE -------------------------*/
    $('#inline-validate').validate({
        rules: {
            required: "required",
            email: {
                required: true,
                email: true
            },
            date: {
                required: true,
                date: true
            },
            url: {
                required: true,
                url: true
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            agree: "required",
            minsize: {
                required: true,
                minlength: 3
            },
            maxsize: {
                required: true,
                maxlength: 6
            },
            minNum: {
                required: true,
                min: 3
            },
            maxNum: {
                required: true,
                max: 16
            }
        },
        errorClass: 'help-block col-lg-6',
        errorElement: 'span',
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
        }
    });


    $('#form-arsip').validate({
        rules: {
            tgl: {
				 required: true,
				 date: true
			},
            nomor: {
                required: true
            },
			dari: {
                required: true
            },
			perihal: {
                required: true
            },
            
        },
		messages:{
			tgl:{
				required:"Tanggal masuk harus diisi",
				date:"Format salah"
				},
			nomor:{
				required:"Nomor harus diisi"
			},
			dari: {
				required:"Pengirim harus diisi"
			},
			perihal:{
				required:"Perihal harus dipilih"
			}
		},
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
        }
    });
    /*----------- END validate CODE -------------------------*/
	$('#form-profil').validate({
        rules: {
            username: {
				 required: true
			},
            password: {
                required: true
            }         
        },
		messages:{
			username:{
				required:"Username tidak boleh kosong"
				},
			password:{
				required:"Masukkan Password Untuk Merubah Username"
			}
		},
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
        }
    });
    /*----------- END validate CODE -------------------------*/
	$('#form-profile').validate({
        rules: {
            password: {
				 required: true
			},
            password_br: {
                required: true,
				minlength: 8
            },
			password_ul: {
                required: true,
                minlength: 8,
                equalTo: "#password_br"
            }
        },
		messages:{
			password:{
				required:"Masukkan Password Lama Untuk Merubah Password"
				},
			password_br:{
				required:"Masukkan Password Baru",
				minlength:"Minimal 8 Karakter"
			},
			password_ul:{
				required:"Ulangi Password Baru",
				minlength:"Minimal 8 Karakter",
				equalTo:"Ulangi Password Yang Sama"
			}
		},
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
        }
    });
    /*----------- END validate CODE -------------------------*/
	$('#form-member').validate({
        rules: {
            namauser: {
				 required: true
			},
            pswd: {
                required: true,
				minlength: 8
            },
			pswd_ul: {
                required: true,
                minlength: 8,
                equalTo: "#pswd"
            }
        },
		messages:{
			namauser:{
				required:"Username tidak boleh kosong"
				},
			pswd:{
				required:"Password tidak boleh kosong",
				minlength:"Minimal 8 karakter"
			},
			pswd_ul:{
				required:"Ulangi password yang sama",
				minlength:"Minimal 8 karakter",
				equalTo:"Ulangi password yang sama"
			}
		},
        errorClass: 'help-block',
        errorElement: 'span',
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-success').addClass('has-error');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error').addClass('has-success');
        }
    });
    /*----------- END validate CODE -------------------------*/
	
	

}