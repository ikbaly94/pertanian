<?php
	session_start();
	error_reporting(0);
		if(isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['password'])){
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!-- BOOTSTRAP STYLES-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	<link rel="stylesheet" href="../assets/validasi/validationEngine.jquery.css" />
</head>

<body>
<center>
<form action="db" method="post" class="form-horizontal" id="form-profile">
	<div class="form-group">
		<div class="col-lg-9">
			<input type="hidden" name="id" value="<?php echo $_SESSION['id']; ?>"/>
			<input type="password" name="password" id="password" style="outline:none; border-top:0px; border-left:0px; border-right:0px; width:100%;" autofocus placeholder="Masukkan Password Lama" required/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-9">
			<input type="password" name="password_br" id="password_br" style="outline:none; border-top:0px; border-left:0px; border-right:0px; width:100%;" autofocus placeholder="Masukkan Password Baru" required/>
		</div>
	</div>
	<div class="form-group">
		<div class="col-lg-9">
			<input type="password" name="password_ul" id="password_ul" style="outline:none; border-top:0px; border-left:0px; border-right:0px; width:100%;" autofocus placeholder="Ulangi Password Baru" required/>
		</div>
	</div>
	<div class="form-actions no-margin-bottom col-lg-9" style="text-align:right; padding-right:10px;">
		<a href="profile" class="btn btn-danger" style="padding:8px 15px;">Batal</a>
		<input type="submit" name="ubahpassword" value="Simpan" class="btn btn-primary" style="padding:8px 15px;" />
	</div>
</form>
</center>
</body>
</html>
<?php
		}else if(!isset($_SESSION['id']) || !isset($_SESSION['username']) || !isset($_SESSION['password'])){
					header('location:../index');
		}

?>
