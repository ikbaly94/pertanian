<?php
	session_start();
	error_reporting(0);
		if(isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['password'])){
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sistem Informasi Pengarsipan Surat : Messages Out</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index">Sistem Informasi<br /> Arsip Surat</a> 
            </div>
					<div class="profile_details">
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<span class="prfil-img"><img src="../assets/img/find_user.png" alt="" width="50" height="50"> </span> 
									<div class="user-name">
										<p><?php echo $_SESSION['username']; ?></p>
										<span>Administrator</span>
									</div>
									<i class="fa fa-angle-down lnr"></i>
									<i class="fa fa-angle-up lnr"></i>
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu">
								<li> <a href="member"><i class="fa fa-cog"></i> Member Setting</a> </li> 
								<li> <a href="profile"><i class="fa fa-user"></i> Profile</a> </li> 
								<li> <a href="../logout"><i class="fa fa-sign-out"></i> Logout</a> </li>
							</ul>
						</li>
					</ul>
					</div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="images/logokbb.png" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a class="" href="index"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
					<li>
                        <a class="" href="messagesin"><i class="fa fa-envelope-o fa-3x"></i> Messages In</a>
                    </li>
					<li>
                        <a class="active-menu" href="messagesout"><i class="fa fa-envelope-o fa-3x"></i> Messages Out</a>
                    </li>
					<li>
                        <a href="form"><i class="fa fa-edit fa-3x"></i> Forms </a>
                    </li>
                   	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Daftar Arsip Surat</h2>   
                        <h5>Selamat datang <?php echo $_SESSION['username']; ?> , Senang melihat anda kembali. </h5>
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             Daftar Arsip Surat
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Keluar</th>
                                            <th>Kode / Nomor Surat</th>
                                            <th>Ke</th>
                                            <th>Perihal</th>
											<th>Keterangan</th>
											<th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php
											include('../koneksi.php');
											$no=1;
											$query = mysql_query("SELECT * FROM messages WHERE kategori='keluar'");
											while($data=mysql_fetch_array($query))
											{
										?>
                                        <tr class="odd gradeX">
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo "$data[tgl]"; ?></td>
                                            <td><?php echo "$data[no_surat]"; ?></td>
                                            <td class="center"><?php echo "$data[dari]"; ?></td>
                                            <td class="center"><?php echo "$data[perihal]"; ?></td>
											<td class="center"><?php echo "$data[ket]"; ?></td>
											<td class="center">
												<?php echo "<a href='formupdate?no_data=$data[no]' class='btn btn-primary'>"; echo "<i class='fa fa-edit'></i> Edit</a>"; ?>
												<a href="db?no_data=<?php echo $data[no];?>" onClick="return confirm('Yakin data surat dengan no <?php echo $data['no_surat'];  ?> akan dihapus ?');" class="btn btn-danger"><i class='fa fa-trash'></i> Hapus</a>
											</td>
                                        </tr>
                                         <?php
										 	$no++;
											} ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                    <!--End Advanced Tables -->
                </div>
            </div>
            

        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    <!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
    
   
</body>
</html>
<?php
		}else if(!isset($_SESSION['id']) || !isset($_SESSION['username']) || !isset($_SESSION['password'])){
					header('location:../index');
		}

?>
