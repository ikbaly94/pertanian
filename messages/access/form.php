<?php
	session_start();
	error_reporting(0);
		if(isset($_SESSION['id']) && isset($_SESSION['username']) && isset($_SESSION['password'])){
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sistem Informasi Pengarsipan Surat : Form</title>
	<!-- BOOTSTRAP STYLES-->
    <link href="../assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FONTAWESOME STYLES-->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
     <!-- MORRIS CHART STYLES-->
   
        <!-- CUSTOM STYLES-->
    <link href="../assets/css/custom.css" rel="stylesheet" />
     <!-- GOOGLE FONTS-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
     <!-- TABLE STYLES-->
    <link href="../assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
	<link rel="stylesheet" href="../assets/validasi/validationEngine.jquery.css" />
	<link rel="stylesheet" href="../assets/datepicker/dist/datepicker.css">
	
	
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index">Sistem Informasi<br /> Arsip Surat</a> 
            </div>
					<div class="profile_details">
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<span class="prfil-img"><img src="../assets/img/find_user.png" alt="" width="50" height="50"> </span> 
									<div class="user-name">
										<p><?php echo $_SESSION['username']; ?></p>
										<span>Administrator</span>
									</div>
									<i class="fa fa-angle-down lnr"></i>
									<i class="fa fa-angle-up lnr"></i>
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu">
								<li> <a href="member"><i class="fa fa-cog"></i> Member Setting</a> </li> 
								<li> <a href="profile"><i class="fa fa-user"></i> Profile</a> </li> 
								<li> <a href="../logout"><i class="fa fa-sign-out"></i> Logout</a> </li>
							</ul>
						</li>
					</ul>
					</div>
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
				<li class="text-center">
                    <img src="images/logokbb.png" class="user-image img-responsive"/>
					</li>
				
					
                    <li>
                        <a href="index"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
                    </li>
					<li>
                        <a class="" href="messagesin"><i class="fa fa-envelope-o fa-3x"></i> Messages In</a>
                    </li>
					<li>
                        <a class="" href="messagesout"><i class="fa fa-envelope-o fa-3x"></i> Messages Out</a>
                    </li>
					<li  >
                        <a class="active-menu" href="form"><i class="fa fa-edit fa-3x"></i> Forms </a>
                    </li>
                   	
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2>Form Arsip Surat</h2>   
                        <h5>Selamat datang <?php echo $_SESSION['username']; ?> , Senang melihat anda kembali. </h5>
                       
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
            <div class="row">
                                <div class="col-md-12">
                                    <center><h3>Form Arsip Surat</h3></center><br />
                                    <form action="db" method="post" class="form-horizontal" id="form-arsip">
										<div class="form-group">
                                            <label class="control-label col-lg-4">Kategori<span> *</span></label>
											<div class="col-lg-3">
                                            	<select name="kategori" class="form-control">
													<option value="masuk">Masuk</option>
													<option value="keluar">Keluar</option>
												</select>
											</div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-lg-4">Tanggal Masuk / Keluar<span> *</span></label>
											<div class="col-lg-3">
                                            	<input type="text" class="form-control docs-date" name="tgl" id="tgl" required/>
										</div>
											
										<div class="form-group">
                                            <label class="control-label col-lg-4">Nomor Surat<span> *</span></label>
											<div class="col-lg-4">
                                            	<input type="text" class="form-control" name="nomor" id="nomor" required/>
											</div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-lg-4">Pengirim / Penerima<span> *</span></label>
											<div class="col-lg-4">
                                            	<input type="text" class="form-control" name="dari" id="dari" required/>
											</div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-lg-4">Perihal<span> *</span></label>
											<div class="col-lg-4">
                                            	<input type="text" class="form-control" name="perihal" id="perihal" required />
											</div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-lg-4">Keterangan<span></span></label>
											<div class="col-lg-4">
                                            	<textarea class="form-control" name="ket"></textarea>
											</div>
                                        </div>
										<div class="form-group">
                                            <label class="control-label col-lg-4">Kode Surat<span> *</span></label>
											<div class="col-lg-4">
                                            	<input type="text" class="form-control" name="kodesurat" id="kodesurat" required/>
											</div>
                                        </div>
										<div class="form-actions no-margin-bottom col-lg-8" style="text-align:right; padding-right:10px;">
											<input type="reset" class="btn btn-danger" value="Batal" style="padding:8px 15px;"/>
                                            <input type="submit" name="simpandata" value="Simpan" class="btn btn-primary" style="padding:8px 15px;" />
                                        </div>
									</form>
								</div>
			</div>
            

        </div>
               
    </div>
             <!-- /. PAGE INNER  -->
            </div>
         <!-- /. PAGE WRAPPER  -->
     <!-- /. WRAPPER  -->
    <!-- SCRIPTS -AT THE BOTOM TO REDUCE THE LOAD TIME-->
    
    <script src="../assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="../assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="../assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="../assets/js/custom.js"></script>
	
	<!-- JQUERY SCRIPTS -->
    <script src="../assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="../assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
	<script src="../assets/datepicker/dist/datepicker.js"></script>
	<script src="../assets/datepicker/js/main.js"></script>
	
	<script src="../assets/validasi/jquery.validationEngine.js"></script>
    <script src="../assets/validasi/jquery.validationEngine-en.js"></script>
    <script src="../assets/validasi/jquery.validate.min.js"></script>
    <script src="../assets/validasi/validationInit.js"></script>
    <script>
        $(function () { formValidation(); });
    </script>
    
   
</body>
</html>
<?php
		}else if(!isset($_SESSION['id']) || !isset($_SESSION['username']) || !isset($_SESSION['password'])){
					header('location:../index');
		}

?>
